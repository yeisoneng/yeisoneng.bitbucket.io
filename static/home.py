from browser import window
from mdcframework.mdc import MDCTopAppBar, MDCComponent, MDCLayoutGrid, MDCButton, MDCChipSet, MDCFormField, MDCForm, MDCFab
# from mdcframework.MyImporter import MDCTopAppBar

# from pythoncore import PythonCore
# Piton = PythonCore()

from radiant import AndroidMain
androidmain = AndroidMain()

from mdcframework.base import MDCView

from browser import html

from content import SIDEBAR, BODY


########################################################################
class Home(MDCView):
    """"""

    # ----------------------------------------------------------------------
    def __init__(self, parent):
        """"""
        super().__init__(parent)

        # self.toolbar.mdc['icon'].bind('click', lambda ev:self.main.drawer.mdc.open())
        # self.icon_button.bind('click', lambda ev:self.main.drawer.mdc.open())

    # ----------------------------------------------------------------------

    def build(self):
        """"""
        parent = html.DIV()
        topappbar = MDCTopAppBar('Radiant Framework')
        topappbar.style = {'display': None, }
        # topappbar.mdc.add_item('info')
        # topappbar.mdc.add_item('print')
        # topappbar.mdc.add_item('bookmark')
        parent <= topappbar
        container = html.DIV(Class='', style={
            # 'padding': '15px',
            # 'padding-top': 'calc(56px + 15px)',
            # 'background-color': '#4db6ac',
            # 'color': 'white',
            'height': '-webkit-fill-available',
            'width': '-webkit-fill-available',
            # 'width': '70%',
            # 'margin': '0px auto',
            # 'background-color': '#4db6ac',
        })
        parent <= container

        layoutgrid = MDCLayoutGrid()

        grid = layoutgrid.mdc.grid()

        cell = grid.mdc.cell(desktop=2, tablet=0, mobile=0)

        cell_sidebar = grid.mdc.cell(desktop=2, tablet=8, mobile=4, Class="mdc-card sidebar-cv")
        # cell_sidebar.style = {'background-color': '#5da4d9', 'overflow': 'hidden', }
        cell_sidebar.style = {'background': 'linear-gradient(135deg,#56aae8 0,#4277d4)', 'overflow': 'hidden', }
        # background: linear-gradient(135deg,#56aae8 0,#4277d4);

        cell_body = grid.mdc.cell(desktop=6, tablet=8, mobile=4)
        # cell_body.style = {'background-color': 'blue', 'height': '50px', }

        cell = grid.mdc.cell(desktop=2, tablet=0, mobile=0)

        self.build_sidebar(cell_sidebar)
        self.build_body(cell_body)

        container <= layoutgrid

        # for link in content.select("a.my_link"):
            # print(link.href)
            # link.bind('click', self.open_link(link))

        return parent

    # ----------------------------------------------------------------------
    def build_body(self, parent):
        """"""

        tabs_ = BODY

        for title, icon, tabs in tabs_:
            parent <= self.add_card(title, icon, tabs)

    # ----------------------------------------------------------------------
    def build_sidebar(self, parent):
        """"""

        me = html.DIV()

        me <= html.IMG(src="http://www.gravatar.com/avatar/9fa2e5df40aba92507f5a2ea7b3c3d63?s=512",
                       style={'width': '100%'
                              },
                       )

        me.style = {



        }

        parent <= me

        label = MDCComponent(html.SPAN('Yeison N. Cardona A.'), style={
            'font-weight': '400',
            # 'position': 'relative',
            'margin-top': '-54px',
            'margin-left': '15px',
            'margin-bottom': '0px',
            'z-index': '10',

            'color': 'white',
        })
        label.mdc.typography('headline5')
        parent <= label
        # parent <= html.BR()

        label = MDCComponent(html.SPAN('Python Ninja'), style={
            'font-weight': '300',
            # 'position': 'relative',
            'margin-top': '-10px',
            # 'bottom': '65px',
            'margin-left': '15px',
            'color': 'white',
            'z-index': '10',
        })
        label.mdc.typography('headline6')
        parent <= label

        parent <= html.DIV(Class='photo-shadow')

        # <img src="http://www.gravatar.com/avatar/9fa2e5df40aba92507f5a2ea7b3c3d63?s=512" style="border-radius: 50%;
        # width: 512px;
        # margin: 0px auto;
        # max-width: 512px;
        # display: block;" alt="">

        tabs_ = SIDEBAR

        while tabs_:

            icon, tabs = tabs_.pop(0)

            card = self.add_card('', icon, tabs, size_icon=150, w=(1, 10))
            card.style = {
                'background-color': 'transparent',
                'color': 'white',
                'box-shadow': None,
            }

            if tabs_:
                card <= html.HR(style={
                    'color': 'transparent',
                    'width': '75%',
                    'margin-right': '0',
                    'height': '10px',
                    'border-color': 'transparent',
                    'border-bottom': '1px solid #7db6e1',
                })

            parent <= card

    # ----------------------------------------------------------------------

    def add_card(self, title, icon, tabs=[], size_icon=150, w=(1, 10)):
        """"""

        layoutgrid = MDCLayoutGrid(style={'width': '100%', 'overflow': 'hidden'})

        grid = layoutgrid.mdc.grid()

        cell = grid.mdc.cell(C=w[0], desktop=w[0], tablet=4, mobile=12)

        if not title:
            # label = MDCComponent(html.SPAN(title, style={'margin-left': '15px'}))
            # label.mdc.typography('headline5')
            # cell <= label
            cell <= html.BR()

        if not title:

            cell <= MDCButton(icon=icon,
                              style={
                                  'margin': '0',
                                  'padding': '0',
                                  'margin-top': '-15px',
                                  'overflow': 'visible',
                              },

                              style_icon={

                                  'font-size': f'{200}px',
                                  'line-height': f'{50}px',
                                  'margin-left': f'-{200}px',
                                  'color': 'white',
                                  'z-index': 1,
                                  'opacity': '0.07'
                              },

                              )
        else:

            cell <= MDCButton(icon=icon,
                              style={
                                  'margin': '0',
                                  'padding': '0',
                                  'margin-top': '-15px',
                                  'overflow': 'visible',
                              },

                              style_icon={

                                  'font-size': f'{500}px',
                                  'line-height': f'{150}px',
                                  'margin-left': f'{-250}px',
                                  'color': '#000000',
                                  'z-index': 1,
                                  'opacity': '0.03'

                                  # font-size: 1000px;
                                  # line-height: 300px;
                                  # margin-left: -500px;
                                  # color: rgb(0, 0, 0);
                                  # opacity: 0.03;
                                  # z-index: 1;


                              },

                              )

        cell = grid.mdc.cell(C=w[1], desktop=w[1], tablet=8, mobile=12, style={'z-index': 10})

        if title:
            label = MDCComponent(html.SPAN(title, style={'margin-left': '15px'}))
            label.mdc.typography('headline5')
            cell <= label
            cell <= html.BR()
            cell <= html.BR()

        while tabs:

            tab = tabs.pop(0)

            tab_ = html.DIV()

            if tabs:
                tab_.style = {

                    'margin-right': '-25px',
                    'border-bottom': '1px solid #e0e0e0',
                    # 'padding': '20px 15px',

                }
            else:
                tab_.style = {

                    'margin-right': '-25px',
                    # 'padding': '20px 15px',

                }

            if not title:
                tab_.style = {
                    'margin-right': 'unset',
                    'padding-top': '0px',
                    'border-bottom': '0',
                }
            else:
                tab_.style = {

                    # 'margin-right': '-25px',
                    'padding': '20px 15px',

                }

            if 'title' in tab:
                label = MDCComponent(html.SPAN(tab['title'], style={'font-weight': '400', 'z-index': 2, }))
                label.mdc.typography('headline6')
                tab_ <= label
                tab_ <= html.BR()

            if 'title2' in tab:
                label = MDCComponent(html.SPAN(tab['title2'], style={'font-weight': '400', 'font-size': '100%', 'z-index': 2}))
                label.mdc.typography('subtitle1')
                tab_ <= label
                tab_ <= html.BR()

            if 'subtitle' in tab:
                label = MDCComponent(html.SPAN(tab['subtitle'], style={'z-index': 2}))
                label.mdc.typography('overline')
                tab_ <= label
                tab_ <= html.BR()

            if 'description' in tab:
                label = MDCComponent(html.SPAN(tab['description'], style={'z-index': 2}))
                label.mdc.typography('body2')
                tab_ <= label

            if 'description2' in tab:
                label = MDCComponent(html.SPAN(tab['description2'], style={'font-weight': '300', 'z-index': 2}))
                label.mdc.typography('body2')
                tab_ <= label

            if 'chips' in tab:

                tab_ <= html.BR()

                for chip in tab['chips']:

                    if chip:
                        tab_ <= html.BR()
                        label = MDCComponent(html.SPAN(chip, style={'font-weight': '300'}))
                        label.mdc.typography('subtitle')
                        tab_ <= label

                    chips = MDCChipSet()
                    for chip_ in tab['chips'][chip]:
                        chips.mdc.add_chip(chip_)
                    tab_ <= chips

            if 'links' in tab:

                links_parent = html.DIV(style={'text-align': 'center'})
                tab_ <= links_parent

                for icon, link in tab['links']:

                    links_parent <= MDCFab(icon, Class="RDNT-open_url", style={
                        'box-shadow': 'none',
                        # 'background-color': '#5da4d9',
                        'background': 'linear-gradient(135deg,#56aae8 0,#4277d4)',
                        'font-size': '200%',
                        'margin': '0 10px',
                    }, href=link)

                    # if chip:
                        # tab_ <= html.BR()
                        # label = MDCComponent(html.SPAN(chip, style={'font-weight': '300'}))
                        # label.mdc.typography('subtitle')
                        # tab_ <= label

                    # chips = MDCChipSet()
                    # for chip_ in tab['chips'][chip]:
                        # chips.mdc.add_chip(chip_)
                    # tab_ <= chips

            if 'skills' in tab:

                tab_ <= html.BR()

                ff = MDCFormField()
                ff.style = {'width': '100%', 'min-height': '10px', 'height': '10px', 'margin-bottom': '20px'}
                form = MDCForm(formfield=ff)

                for skill, percent in tab['skills']:
                    label = MDCComponent(html.SPAN(skill))
                    label.mdc.typography('body2')
                    # tab_ <= label
                    # tab_ <= html.BR()

                    form <= label
                    form.mdc.Slider('Slider', min=0, max=100, step=1, valuenow=percent, continuous=True, disabled=True)

                tab_ <= form

                    # chips.mdc.add_chip(chip)
                # tab_ <= chips

            cell <= tab_

        card = html.DIV(layoutgrid, Class="mdc-card", style={'width': '100%', 'margin-bottom': '15px'})
        return card

    # ----------------------------------------------------------------------
    def open_link(self, link):
        """"""
        def inset(event):
            event.preventDefault()
            androidmain.open_url(link.href)
        return inset
