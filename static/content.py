SIDEBAR = [
    (
        'person', [{
            # 'title': 'Electronic Engeener',
            # 'subtitle': 'Universidad Nacional de Colombia',
            'description': '"It is very likely that I am programming at this very moment".',
        },
        ]
    ),

    ('email', [
        {
            'title2': 'yeisoneng@gmail.com',
            'description2': 'personal',
        },
        {
            'title2': 'yencardonaal@unal.edu.com',
            'description2': 'academic',
        },
    ]),

    ('phone', [
        {
            'title2': '(+57) 300 872 9952',
            'description2': 'personal',
        },
    ]),

    ('location_city', [
        {
            'title2': 'Manizales. Caldas, Colombia',
            # 'description2': 'personal',
        },
        {
            'title2': 'Viterbo. Caldas, Colombia',
            # 'description2': 'personal',
        },
    ]),


    ('code', [
        {
            'title': 'Programming Languages',
            'skills': [
                ('Python', 99),
                ('JavaScript', 75),
                ('HTML', 90),
                ('CSS', 90),
                ('C++', 60),
                ('C', 60),
                ('Java', 10),
                ('Matlab', 10),
                #('Git', 75),
                #('Mercurial', 90),
            ],
        },
    ]),

    ('developer_board', [
        {
            'title': 'Developer Skills',
            'skills': [
                ('Jupyter', 99),
                ('Sphinx', 80),
                ('Git', 75),
                ('Mercurial', 90),
                ('Scrum', 80),
                #('C', 60),
                #('Java', 10),
                #('Matlab', 10),
            ],
        },
    ]),

]


BODY = [

    ('Education', 'school', [

        {'title': 'Master in Electronic Engineering - Industrial Automation (currently)',
         'subtitle': 'Universidad Nacional de Colombia',
         # 'description': 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. ',
         },

        {'title': 'Electronic Engineer',
         'subtitle': 'Universidad Nacional de Colombia',
         # 'description': 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. ',
         },


    ]),



    ('Areas of interest', 'fas-glasses', [

        {'title': 'EEG, BCI',
         # 'subtitle': 'Fullstack Python Developer',
         'description': 'Acquisition systems, preprocessing and end-user app development.',
         'chips': {'': ['OpenBCI'],
                   },
         },

        {'title': 'Data visualization',
         # 'subtitle': 'Fullstack Python Developer',
         'description': 'For realtime, web, desktop and embedded devices applications.',
         'chips': {'': ['Matplotlib', 'Mpld3', 'ChartJs', 'Plotly'],
                   },
         },

    ]),



    ('Developer', 'code', [

        {'title': 'Python',
         'subtitle': 'Fullstack Python Developer',
         # 'description': 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. ',
         'chips': {'Scientific': ['NumPy', 'SciPy', 'Matplotlib', 'SymPy', 'Pandas', 'Jupyter', 'Scikit-learn', 'OpenCV', 'Mpld3'],
                   'GUIs': ['TkInter', 'PySide', 'PyQt', 'WxPython'],

                   'Web': ['Beautiful Soup', 'Selenium'],

                   },
         },

        {'title': 'Web',
         'subtitle': 'Frontend and Backend Developer',
         # 'description': 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. ',
         'chips': {'': ['HTML', 'CSS', 'JavaScript'],
                   'Fraweworks': ['Bootstrap', 'Material Components Web', 'Django', 'Django REST frameworks', 'Tornado'],
                   'Languages': ['SCSS', 'SASS', 'CoffeeScript', 'Brython'],

                   },
         },


        {'title': 'Linux',
         'subtitle': 'System and Server Administration',
         # 'description': 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. ',
         'chips': {'': ['Desktop', 'Servers'],
                   # 'Fraweworks': ['Bootstrap', 'Material Components Web'],
                   'Distributions': ['ArchLinux', 'ArchLinuxARM', 'Debian', 'Ubuntu'],
                   },
         },

        {'title': 'Embedded Devices & Graphical design',
         'subtitle': 'Prototypes design',
         # 'description': 'Prototypes design, dedicated operating systems deployment, real time data acquisition.',
         'chips': {'': ['Raspberry', 'ESP32', 'Arduino', 'Pinguino', 'ChipKit'],
                   # 'Fraweworks': ['Bootstrap', 'Material Components Web'],
                   'Languages': ['C', 'C++', 'MicroPython'],
                   'Graphical design': ['Inkscape', 'Gimp', 'OpenSCAD', 'Eagle', 'bCNC', ],
                   },
         },


        # {'title': 'Graphical Design',
        # 'subtitle': '',
        # # 'description': 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. ',
        # 'chips': {'': ['Inkscape', 'Gimp', 'OpenSCAD', 'Eagle', 'bCNC', ],
        # # 'Fraweworks': ['Bootstrap', 'Material Components Web'],
        # # 'Languages': ['C', 'C++', 'MicroPython', 'Bash'],
        # },
        # },




    ]),



    ('Social', 'fas-link', [

        {'links': [
            ("fab-twitter", "https://twitter.com/yeisoneng/"),
            ("fab-github", "https://github.com/yeisoncardona/"),
            ("fab-bitbucket", "https://bitbucket.org/yeisoneng/"),
            ("fab-google-play", "https://play.google.com/store/apps/dev?id=8582050006689282112")
        ]
            # 'title': 'Master of Engineering - Industrial Automation (currently)',
            # 'subtitle': 'Universidad Nacional de Colombia',
            # 'description': 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. ',
         },

    ]),

]
